<?php

function my_array_sum(array $array) : int|float {

    for ($s = 0, $i = 0, $c = count($array) ; $i < $c; $i++){
        if (is_int($array[$i]) || is_float($array[$i])){
            $s += $array[$i];
        }
    }
    return $s;
}

$a = [1, 15, 42, 'hjk', 1.25, 'gfd'];
var_dump(my_array_sum($a));
echo '<br>';
var_dump(array_sum($a));       // проверка результатов рабочей функцией

//modify
//pull 
